const parseAuditAdvisory = require('./parseAuditAdvisory');

it('should convert a yarn auditAdvisory into a GitLab dependency scanning report entry', () => {
  const advisory = {
    findings: [
      {
        version: '2.4.2',
        paths: ['lodash'],
        dev: false,
        optional: false,
        bundled: false,
      },
      {
        version: '3.10.1',
        paths: ['surge>cli-table2>lodash'],
        dev: false,
        optional: false,
        bundled: false,
      },
    ],
    id: 782,
    created: '2019-02-13T16:16:53.770Z',
    updated: '2019-02-13T16:16:53.770Z',
    deleted: null,
    title: 'Prototype Pollution',
    found_by: { link: '', name: 'asgerf' },
    reported_by: { link: '', name: 'asgerf' },
    module_name: 'lodash',
    cves: ['CVE-2018-16487'],
    vulnerable_versions: '<4.17.11',
    patched_versions: '>=4.17.11',
    overview:
      "Versions of `lodash` before 4.17.5 are vulnerable to prototype pollution. \n\nThe vulnerable functions are 'defaultsDeep', 'merge', and 'mergeWith' which allow a malicious user to modify the prototype of `Object` via `{constructor: {prototype: {...}}}` causing the addition or modification of an existing property that will exist on all objects.\n\n",
    recommendation: 'Update to version 4.17.11 or later.',
    references: '- [HackerOne Report](https://hackerone.com/reports/380873)',
    access: 'public',
    severity: 'moderate',
    cwe: 'CWE-471',
    metadata: { module_type: '', exploitability: 3, affected_components: '' },
    url: 'https://npmjs.com/advisories/782',
  };
  const result = parseAuditAdvisory(advisory);
  expect(result).toStrictEqual({
    confidence: 'High',
    description:
      "Versions of `lodash` before 4.17.5 are vulnerable to prototype pollution. \n\nThe vulnerable functions are 'defaultsDeep', 'merge', and 'mergeWith' which allow a malicious user to modify the prototype of `Object` via `{constructor: {prototype: {...}}}` causing the addition or modification of an existing property that will exist on all objects.\n\n",
    identifiers: [
      {
        name: 'CVE-2018-16487',
        url: 'https://nvd.nist.gov/vuln/detail/CVE-2018-16487',
      },
      {
        name: 'CWE-471',
        url: 'https://cwe.mitre.org/data/definitions/471',
      },
    ],
    instances: [{ method: 'lodash' }, { method: 'surge>cli-table2>lodash' }],
    links: [
      { url: 'https://hackerone.com/reports/380873' },
      { url: 'https://npmjs.com/advisories/782' },
    ],
    location: { file: 'yarn.lock' },
    message: 'Prototype Pollution',
    namespace: 'lodash',
    severity: 'Moderate',
    solution: 'Update to version 4.17.11 or later.',
  });
});
